import React, { ReactElement, useReducer } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import LoginView from './views/LoginView';
import CarPatentConsultView from './views/CarPlateConsultView';
import ResidentVisitView from './views/ResidentVisitView';
import getGlobalContext from './store/getGlobalContext';
import { reducer, initialState } from './store/reducer';
import GlobalContext from './store/GlobalContext';

// Components
import Loader from './components/Loader';

function App(): ReactElement {
  // Context
  const [state, dispatch] = useReducer(reducer, initialState);
  const globalContext = getGlobalContext(dispatch);

  return (
    <GlobalContext.Provider value={{ state, context: globalContext }}>
      <Router>
        <Switch>
          <Route exact path="/">
            <LoginView />
            {state.isLoading && <Loader />}
          </Route>
          {(state.token.length > 0) && (
            <Route exact path="/consult">
              <CarPatentConsultView />
              {state.isLoading && <Loader />}
            </Route>
          )}
          {(state.token.length > 0) && (
            <Route exact path="/resident">
              <ResidentVisitView />
              {state.isLoading && <Loader />}
            </Route>
          )}
        </Switch>
      </Router>
    </GlobalContext.Provider>
  );
}
export default App;
