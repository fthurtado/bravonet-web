/* eslint-disable jsx-a11y/label-has-associated-control */
import React, {
  ReactElement,
  useContext,
  useEffect,
  useState,
} from 'react';

import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import moment from 'moment';
import Select from 'react-select';

// Icons
import { Close } from '@material-ui/icons';

// Interfaces
import {
  ContextInterface,
  PlateRegistrationModalProps,
  HouseData,
  // SelectOption,
} from '../interfaces';

// Helpers
import checkRut from '../helpers/checkRut';
import phoneRegex from '../helpers/phoneRegex';
import rutRegex from '../helpers/rutRegex';

// Styles
import '../styles/PlateRegistrationModalStyles.scss';

// Components
import Warning from './Warning';

// Context
import GlobalContext from '../store/GlobalContext';

function PlateRegistrationModal(props: PlateRegistrationModalProps): ReactElement {
  // Props
  const {
    visible,
    carPlate,
    state,
  } = props;

  // Context
  const { context }: { context: ContextInterface } = useContext(GlobalContext);
  const {
    addVisitor,
    addService,
    getResidences,
  } = context;

  // States
  const [type, setType] = useState<string>('visitor');
  // const [selectedOrder, setSelectedOrder] = useState<SelectOption>({ value: -1, label: '' });

  // Visit states
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [phone, setPhone] = useState<string>('+56 ');
  const [email, setEmail] = useState<string>('');
  const [rut, setRut] = useState<string>('');
  const [isValidRut, setIsValidRut] = useState<boolean>(false);
  const [showWarning, setShowWarning] = useState<boolean>(false);
  const [showSuccess, setShowSuccess] = useState<boolean>(false);
  const [showRegisterError, setShowRegisterError] = useState<boolean>(false);
  const [showGetResidencesError, setShowGetResidencesError] = useState<boolean>(false);
  const [selectedDateIn, handleDateInChange] = useState<Date | null>(new Date());
  const [selectedDateOut, handleDateOutChange] = useState<Date | null>(new Date());
  const [residence, setResidence] = useState<HouseData>({
    id: 0,
    identifier: '',
  });
  const [residences, setResidences] = useState<HouseData[]>([]);

  // Services states
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [recurrency, setRecurrency] = useState<string>('0000000');

  function handleSubmit(): void {
    if (type === 'visitor') {
      if (!isValidRut
        || !(firstName.length > 0)
        || !(lastName.length > 0)
        || !(phone.length > 4)
        || !(email.length > 0)) {
        setShowWarning(true);
        return;
      }
      const data = {
        type,
        firstName,
        lastName,
        phone,
        email,
        rut,
        startDate: moment(selectedDateIn).format('YYYY-MM-DD HH:mm:ss'),
        endDate: moment(selectedDateOut).format('YYYY-MM-DD HH:mm:ss'),
        house: residence.identifier,
        plate: carPlate,
      };
      addVisitor(state.token, data)
        .then(() => {
          setShowSuccess(true);
        })
        .catch(() => {
          setShowRegisterError(true);
        });
    } else {
      if (!(name.length > 0)
      || !(description.length > 0)
      || !(recurrency !== '0000000')) {
        setShowWarning(true);
        return;
      }
      const data = {
        type,
        name,
        description,
        recurrency,
        house: residence.identifier,
        plate: carPlate,
      };
      addService(state.token, data)
        .then(() => {
          setShowSuccess(true);
        })
        .catch(() => {
          setShowRegisterError(true);
        });
    }
  }

  function changeRecurrency(day: number) {
    const newRecurrency = (recurrency[day] === '0')
      ? `${recurrency.substring(0, day)}1${recurrency.substring(day + 1)}`
      : `${recurrency.substring(0, day)}0${recurrency.substring(day + 1)}`;
    setRecurrency(newRecurrency);
  }

  useEffect(() => {
    const response = checkRut(rut);
    setIsValidRut(response);
  }, [rut]);

  useEffect(() => {
    getResidences(state.token)
      .then((res) => {
        setResidences(res.houses);
      })
      .catch((err) => {
        setShowGetResidencesError(true);
      });
  }, []);

  if (showWarning) {
    return (
      <Warning
        visible={setShowWarning}
        message="Debes completar todo el formulario"
        buttonMessage="Volver"
      />
    );
  }

  if (showRegisterError) {
    return (
      <Warning
        visible={setShowRegisterError}
        message="A ocurrido un error con el registro, intentelo nuevamente"
        buttonMessage="Volver"
      />
    );
  }

  if (showSuccess) {
    return (
      <Warning
        visible={visible}
        message="Registro completado correctamente"
        buttonMessage="Aceptar"
      />
    );
  }

  if (showGetResidencesError) {
    return (
      <Warning
        visible={visible}
        message="A ocurrido un error, intentelo nuevamente"
        buttonMessage="Aceptar"
      />
    );
  }

  return (
    <div
      className="modal"
      id="MyModal"
    >
      <div className="modal-content">
        <Close
          className="modal-close"
          onClick={() => visible(false)}
        />
        <div className="modal-main">
          <p className="title">
            Registro de nueva patente
          </p>
          <select className="select" name="type" id="type" onChange={(e) => setType(e.target.value)}>
            <option value="visitor">Visita</option>
            <option value="service">Servicio</option>
          </select>
          <div className="input-container">
            <p className="label">
              Patente:
            </p>
            <div className="input-message-container">
              <input
                type="text"
                value={carPlate}
                readOnly
              />
            </div>
          </div>
          {(type === 'visitor')
            ? (
              <div className="inputs-container">
                <div className="left">
                  <div className="input-container">
                    <p className="label">
                      Nombre:
                    </p>
                    <div className="input-message-container">
                      <input
                        type="text"
                        value={firstName}
                        placeholder="Escriba un nombre..."
                        onChange={(e) => setFirstName(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Apellidos:
                    </p>
                    <div className="input-message-container">
                      <input
                        type="text"
                        value={lastName}
                        placeholder="Escriba un apellido..."
                        onChange={(e) => setLastName(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Teléfono:
                    </p>
                    <div className="input-message-container">
                      <input
                        type="text"
                        value={phone}
                        placeholder="Escriba un celular (+56 9 1111 1111)"
                        onChange={(e) => setPhone(phoneRegex(e.target.value))}
                      />
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Email:
                    </p>
                    <div className="input-message-container">
                      <input
                        type="text"
                        value={email}
                        placeholder="Escriba un email..."
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
                <div className="right">
                  <div className="input-container">
                    <p className="label">
                      Rut:
                    </p>
                    <div className="input-message-container">
                      <input
                        type="text"
                        value={rut}
                        placeholder="Escriba un rut (11.111.111-1)"
                        onChange={(e) => setRut(rutRegex(e.target.value))}
                      />
                      <p className="warning">
                        {(!isValidRut && rut.length > 0) && 'Rut inválido (11.111.111-1)' }
                      </p>
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Fecha de Ingreso:
                    </p>
                    <div className="input-message-container">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DateTimePicker
                          value={selectedDateIn}
                          onChange={handleDateInChange}
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Fecha de Salida:
                    </p>
                    <div className="input-message-container">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DateTimePicker
                          value={selectedDateOut}
                          onChange={handleDateOutChange}
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Residencia:
                    </p>
                    <Select
                      className="select-container"
                      classNamePrefix="select"
                      isSearchable
                      value={residence.id >= 0 ? ({ value: residence.id, label: residence.identifier }) : null}
                      components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                      placeholder="Residencia..."
                      options={residences.map((res) => ({ value: res.id, label: res.identifier }))}
                      onChange={(e) => { if (e) { setResidence({ id: e.value, identifier: e.label }); } }}
                      // onFocus={() => setSearchResidence(storeResidence.name)}
                      // onMenuClose={() => setSearchResidence(storeResidence.name)}
                      // defaultInputValue={searchResidence}
                      // inputValue={searchResidence}
                      // onInputChange={(text) => setSearchResidence(text)}
                    />
                  </div>
                </div>
              </div>
            ) : (
              <div className="inputs-container">
                <div className="left">
                  <div className="input-container">
                    <p className="label">
                      Nombre:
                    </p>
                    <div className="input-message-container">
                      <input
                        type="text"
                        value={name}
                        placeholder="Escriba un nombre..."
                        onChange={(e) => setName(e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Descripción:
                    </p>
                    <div className="input-message-container">
                      <textarea
                        value={description}
                        placeholder="Descripción del servicio..."
                        onChange={(e) => setDescription(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
                <div className="right">
                  <div className="input-container">
                    <p className="label">
                      Recurrencia:
                    </p>
                    <div className="input-checkbox">
                      <div className="option">
                        <input
                          type="checkbox"
                          name="monday"
                          value="monday"
                          id="monday"
                          onChange={() => changeRecurrency(0)}
                        />
                        <label htmlFor="monday" />
                        <label>Lunes</label>
                      </div>
                      <div className="option">
                        <input
                          type="checkbox"
                          name="tuesday"
                          value="tuesday"
                          id="tuesday"
                          onChange={() => changeRecurrency(1)}
                        />
                        <label htmlFor="tuesday" />
                        <label>Martes</label>
                      </div>
                      <div className="option">
                        <input
                          type="checkbox"
                          name="wednesday"
                          value="wednesday"
                          id="wednesday"
                          onChange={() => changeRecurrency(2)}
                        />
                        <label htmlFor="wednesday" />
                        <label>Miércoles</label>
                      </div>
                      <div className="option">
                        <input
                          type="checkbox"
                          name="thursday"
                          value="thursday"
                          id="thursday"
                          onChange={() => changeRecurrency(3)}
                        />
                        <label htmlFor="thursday" />
                        <label>Jueves</label>
                      </div>
                      <div className="option">
                        <input
                          type="checkbox"
                          name="friday"
                          value="friday"
                          id="friday"
                          onChange={() => changeRecurrency(4)}
                        />
                        <label htmlFor="friday" />
                        <label>Viernes</label>
                      </div>
                      <div className="option">
                        <input
                          type="checkbox"
                          name="saturday"
                          value="saturday"
                          id="saturday"
                          onChange={() => changeRecurrency(5)}
                        />
                        <label htmlFor="saturday" />
                        <label>Sábado</label>
                      </div>
                      <div className="option">
                        <input
                          type="checkbox"
                          name="sunday"
                          value="sunday"
                          id="sunday"
                          onChange={() => changeRecurrency(6)}
                        />
                        <label htmlFor="sunday" />
                        <label>Domingo</label>
                      </div>
                    </div>
                  </div>
                  <div className="input-container">
                    <p className="label">
                      Residencia:
                    </p>
                    {/* <div className="input-message-container">
                      <input
                        type="text"
                        value={residence}
                        placeholder="Número de la residencia..."
                        onChange={(e) => setResidence(e.target.value)}
                      />
                    </div> */}
                  </div>
                </div>
              </div>
            )}
          <button
            type="button"
            onClick={() => handleSubmit()}
          >
            Aceptar
          </button>
        </div>
      </div>
    </div>
  );
}

export default PlateRegistrationModal;
