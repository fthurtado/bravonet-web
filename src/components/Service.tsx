import React, { ReactElement } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';

// Style
import '../styles/Visit.scss';

// Interfaces
import {
  ServiceProps,
// SelectOption,
} from '../interfaces';

function Service(props: ServiceProps): ReactElement {
  const {
    name,
    recurrency,
    plate,
  } = props;
  const semana = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  const dias = recurrency.split('');
  let count = -1;
  let a = false;
  return (
    <div className="container">
      <div className="container_row">
        <p className="name">{name}</p>
      </div>
      <div className="container_row">
        {dias.map((e) => {
          if ((e === '1') && !a) {
            count += 1;
            a = true;
            return (
              <div className="container_row">
                <p className="name">Dias:&nbsp;</p>
                <p className="name">{semana[count]}</p>
                <p className="name">&nbsp;</p>
              </div>
            );
          }
          if (e === '1') {
            count += 1;
            return (
              <div className="container_row">
                <p className="name">{semana[count]}</p>
                <p className="name">&nbsp;</p>
              </div>
            );
          }
          count += 1;
          return false;
        })}
      </div>
      <div className="container_row">
        <p>Patente:&nbsp;</p>
        <p>{plate}</p>
      </div>
    </div>
  );
}

export default Service;
