import React, { ReactElement } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';

// Style
import '../styles/Visit.scss';

// Interfaces
import {
  VisitProps,
// SelectOption,
} from '../interfaces';

function Visit(props: VisitProps): ReactElement {
  const {
    name,
    lastName,
    arrive,
    leave,
    patent,
  } = props;
  return (
    <div className="container">
      <div className="container_row">
        <p className="name">{name}</p>
        <p className="name">&nbsp;</p>
        <p className="name">{lastName}</p>
      </div>
      <div className="container_row">
        <p>Fecha llegada:&nbsp;</p>
        <p>{moment(arrive).format('YYYY-MM-DD HH:mm:ss')}</p>
      </div>
      <div className="container_row">
        <p>Fecha salida:&nbsp;</p>
        <p>{moment(leave).format('YYYY-MM-DD HH:mm:ss')}</p>
      </div>
      <div className="container_row">
        <p>Patente:&nbsp;</p>
        <p>{patent}</p>
      </div>
    </div>
  );
}

export default Visit;
