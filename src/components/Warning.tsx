import React, { ReactElement } from 'react';

// Interfaces
import { WarningProps } from '../interfaces';

// Styles
import '../styles/WarningStyles.scss';

function Warning(props: WarningProps): ReactElement {
  const {
    visible,
    message,
    buttonMessage,
  } = props;

  return (
    <div
      className="modal-warning"
      id="MyModal"
    >
      <div className="modal-content">
        <div className="modal-main">
          <p className="message">
            {message}
          </p>
          <button
            type="button"
            onClick={() => visible(false)}
          >
            {buttonMessage}
          </button>
        </div>
      </div>
    </div>
  );
}

export default Warning;
