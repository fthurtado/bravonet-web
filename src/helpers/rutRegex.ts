export default function rutRegex(value: string): string {
  let rutRegexValue = value.replace(/[^kK\d]/g, '');
  if (rutRegexValue.match(/^(\d{1,3})(\w{1})$/)) {
    rutRegexValue = rutRegexValue.replace(/^(\d{1,3})(\w{1})$/, '$1-$2');
  } else if (rutRegexValue.match(/^(\d{1,3})(\d{3})(\w{1})$/)) {
    rutRegexValue = rutRegexValue.replace(/^(\d{1,3})(\d{3})(\w{1})$/, '$1.$2-$3');
  } else if (rutRegexValue.match(/^(\d{1,2})(\d{3})(\d{3})(\w{1})$/)) {
    rutRegexValue = rutRegexValue.replace(/^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
  } else {
    rutRegexValue = rutRegexValue.substr(0, 9).replace(/^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
  }
  return rutRegexValue;
}
