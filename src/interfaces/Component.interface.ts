import { StateInterface } from './State.interface';

export interface PlateRegistrationModalProps {
  visible(value: boolean): void;
  carPlate: string;
  state: StateInterface
}

export interface VisitRegistrationModalProps {
  visible(value: boolean): void;
  houseId: string;
  state: StateInterface
}

export interface SelectOption {
  value: number,
  label: string,
}

export interface WarningProps {
  visible(value: boolean): void;
  message: string;
  buttonMessage: string;
}

export interface VisitRegistrationModalProps {
  visible(value: boolean): void;
}

export interface VisitProps {
  name: string;
  lastName: string;
  arrive: string;
  leave: string;
  patent: string;
}

export interface ServiceProps {
  name: string;
  recurrency: string;
  plate: string;
  
}
