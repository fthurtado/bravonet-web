export interface ContextInterface {
  logIn(rut: string, password: string, isStaff: boolean): Promise<void>;
  signOut(): void;
  checkPlateString(token: string, data: CheckPlateStringData): Promise<CheckPlateData>;
  checkPlateImage(token: string, data: CheckPlateImageData): Promise<CheckPlateData>;
  addVisitor(token: string, data: VisitorData): Promise<void>;
  addService(token: string, data: ServiceData): Promise<void>;
  getResidences(token: string): Promise<GetResidencesData>;
  getVisitorService(token: string, id: number): Promise<GetVisitorServiceData>;
}

export interface ReducerActionInterface {
  type: string;
  data?: {
    token?: string;
    isLoading?: boolean;
    houseId: number;
    userFirstName: string;
    userLastName: string;
    type: string;
  }
}

export interface CheckPlateStringData {
  plate: string,
}

export interface CheckPlateImageData {
  image: File,
}

export interface CheckPlateData {
  type: string,
  service: ServiceData,
  visitor: VisitorData,
  house: HouseData,
}

export interface VisitorData {
  type?: string,
  firstName: string,
  lastName: string,
  phone: string,
  email: string,
  rut: string,
  startDate: string,
  endDate: string,
  house: number | string,
  plate: string,
}

export interface ServiceData {
  type?: string,
  name: string,
  description: string,
  recurrency: string,
  house: number | string,
  plate: string,
}

export interface HouseData {
  id: number,
  identifier: string,
}

export interface GetResidencesData {
  houses: HouseData[],
}

export interface PlateData {
  id: number,
  value: string,
}

export interface VisitorData2 {
  id: number,
  plates: PlateData[],
  firstName: string,
  lastName: string,
  phone: string,
  email: string,
  rut: string,
  startDate: string,
  endDate: string,
  createdAt: string,
  updatedAt: string,
  house: number,
}

export interface ServiceData2 {
  id: number,
  plates: PlateData[],
  name: string,
  description: string,
  recurrency: string,
  createdAt: string,
  updatedAt: string,
  house: number,
}

export interface GetVisitorServiceData {
  visitors: VisitorData2[],
  services: ServiceData2[],
}