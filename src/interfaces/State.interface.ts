export interface StateInterface {
  token: string;
  houseId: number;
  userFirstName: string;
  userLastName: string;
  type: string;
  isLoading: boolean;
}
