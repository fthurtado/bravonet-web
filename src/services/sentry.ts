import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

export function initSentry(): void {
  Sentry.init({
    dsn: 'https://9ca724c046d341438d0c2bc040abc9dc@o574107.ingest.sentry.io/5724830',
    integrations: [new Integrations.BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
}

export default initSentry;

// Can also use with React Concurrent Mode
// ReactDOM.createRoot(document.getElementById('root')).render(<App />);
