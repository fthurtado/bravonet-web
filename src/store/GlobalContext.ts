import { createContext } from 'react';
import { StateInterface } from '../interfaces';
import { initialState } from './reducer';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const context: { state: StateInterface, context: any } = { state: initialState, context: undefined };

export default createContext(context);
