import React from 'react';
import axios from 'axios';
import FormData from 'form-data';

// Interfaces
import { ContextInterface, ReducerActionInterface } from '../interfaces';

// Env vars
const {
  REACT_APP_API_URL,
  REACT_APP_API_VERSION,
} = process.env;

function GetGlobalContext(
  dispatch: React.Dispatch<ReducerActionInterface>,
): ContextInterface {
  const globalContext: ContextInterface = {
    logIn: async (rut, password, isStaff) => {
      dispatch({ type: 'LOADING' });
      const route = isStaff
        ? `/administration/${REACT_APP_API_VERSION}/login/staff/`
        : `/administration/${REACT_APP_API_VERSION}/login/resident/`;
      await axios(`${REACT_APP_API_URL}${route}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        data: { rut, password },
      })
        .then((response) => response.data)
        .then((data) => {
          dispatch({ type: 'LOG_IN', data });
          dispatch({ type: 'LOADING' });
        })
        .catch((err) => {
          dispatch({ type: 'LOADING' });
          throw err;
        });
    },
    signOut: () => {
      dispatch({ type: 'SIGN_OUT' });
      window.location.href = '/';
    },
    checkPlateString: async (token, data) => {
      const route = `/plates/${REACT_APP_API_VERSION}/check-plate-string/`;
      const response = await axios({
        method: 'get',
        url: `${REACT_APP_API_URL}${route}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          ...data,
          condoId: 1,
        },
      })
        .then((res) => res.data)
        .catch((err) => {
          throw err;
        });
      return response;
    },
    checkPlateImage: async (token, data) => {
      const route = `/plates/${REACT_APP_API_VERSION}/check-plate-image/`;
      const formData = new FormData();
      formData.append('image', data.image);
      formData.append('condoId', 1);
      const response = await axios({
        method: 'post',
        url: `${REACT_APP_API_URL}${route}`,
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
        data: formData,
      })
        .then((res) => res.data)
        .catch((err) => {
          throw err;
        });
      return response;
    },
    addVisitor: async (token, data) => {
      dispatch({ type: 'LOADING' });
      const route = `/administration/${REACT_APP_API_VERSION}/create-visit/`;
      await axios({
        method: 'post',
        url: `${REACT_APP_API_URL}${route}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          ...data,
        },
      })
        .then(() => {
          dispatch({ type: 'LOADING' });
        })
        .catch((err) => {
          dispatch({ type: 'LOADING' });
          throw err;
        });
    },
    addService: async (token, data) => {
      dispatch({ type: 'LOADING' });
      const route = `/administration/${REACT_APP_API_VERSION}/create-visit/`;
      await axios({
        method: 'post',
        url: `${REACT_APP_API_URL}${route}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          ...data,
        },
      })
        .then(() => {
          dispatch({ type: 'LOADING' });
        })
        .catch((err) => {
          dispatch({ type: 'LOADING' });
          throw err;
        });
    },
    getResidences: async (token) => {
      dispatch({ type: 'LOADING' });
      const route = `/administration/${REACT_APP_API_VERSION}/get-houses/`;
      const residences = await axios({
        method: 'get',
        url: `${REACT_APP_API_URL}${route}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          condoId: 1,
        },
      })
        .then((res) => {
          dispatch({ type: 'LOADING' });

          return res.data;
        })
        .catch((err) => {
          dispatch({ type: 'LOADING' });
          throw err;
        });

      return residences;
    },
    getVisitorService: async (token, id) => {
      dispatch({ type: 'LOADING' });
      const route = `/administration/${REACT_APP_API_VERSION}/get-visitors-and-services/`;
      const residences = await axios({
        method: 'get',
        url: `${REACT_APP_API_URL}${route}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          houseId: id,
        },
      })
        .then((res) => {
          dispatch({ type: 'LOADING' });

          return res.data;
        })
        .catch((err) => {
          dispatch({ type: 'LOADING' });
          throw err;
        });

      return residences;
    },
  };

  return globalContext;
}

export default GetGlobalContext;
