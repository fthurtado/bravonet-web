import { StateInterface, ReducerActionInterface } from '../interfaces';

export const initialState = {
  token: '',
  userFirstName: '',
  userLastName: '',
  houseId: -1,
  type: '',
  isLoading: false,
};

export const reducer = (state: StateInterface, action: ReducerActionInterface): StateInterface => {
  switch (action.type) {
    case 'LOG_IN':
      return {
        ...state,
        token: action.data?.token ? action.data.token : '',
        userFirstName: action.data?.userFirstName ? action.data.userFirstName : '',
        userLastName: action.data?.userLastName ? action.data.userLastName : '',
        houseId: action.data?.houseId ? action.data.houseId : -1,
        type: action.data?.type ? action.data.type : '',
      };
    case 'SIGN_OUT':
      return initialState;
    case 'LOADING':
      return {
        ...state,
        isLoading: !state.isLoading,
      };
    default:
      return state;
  }
};
