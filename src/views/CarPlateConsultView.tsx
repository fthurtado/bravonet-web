import React, {
  ReactElement,
  useContext,
  useEffect,
  useState,
} from 'react';
import moment from 'moment';
import 'moment/locale/es-mx';

import { useHistory } from 'react-router-dom';

// Icons
import {
  Dehaze,
} from '@material-ui/icons';

import {
  CircularProgress,
} from '@material-ui/core';

// Styles
import '../styles/CarPlateConsultStyles.scss';

// Components
import PlateRegistrationModal from '../components/PlateRegistrationModal';
import Warning from '../components/Warning';

// Interfaces
import {
  ContextInterface,
  StateInterface,
  ServiceData,
  VisitorData,
  HouseData,
} from '../interfaces';
import GlobalContext from '../store/GlobalContext';

const weekDays = [
  'Lunes ',
  'Martes ',
  'Miércoles ',
  'Jueves ',
  'Viernes ',
  'Sábado ',
  'Domingo ',
];

function CarPlateConsultView(): ReactElement {
  // Context
  const { state, context }: { state: StateInterface, context: ContextInterface } = useContext(GlobalContext);
  const { checkPlateString, checkPlateImage, signOut } = context;

  // History
  const history = useHistory();

  console.log(state);

  // States
  const [carPlate, setCarPlate] = useState<string>('');
  const [carPlateIsValid, setCarPlateIsValid] = useState<boolean>(false);
  const [loadingInfo, setLoadingInfo] = useState<boolean>(false);
  const [isApiCalled, setIsApiCalled] = useState<boolean>(false);
  const [plateNotFound, setPlateNotFound] = useState<boolean>(false);
  const [plateInfoHouse, setPlateInfoHouse] = useState<HouseData>({
    id: 0,
    identifier: '',
  });
  const [plateInfoVisitor, setPlateInfoVisitor] = useState<VisitorData>({
    type: '',
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    rut: '',
    startDate: '',
    endDate: '',
    house: 0,
    plate: '',
  });
  const [plateInfoService, setPlateInfoService] = useState<ServiceData>({
    type: '',
    name: '',
    description: '',
    recurrency: '',
    house: 0,
    plate: '',
  });
  const [plateType, setPlateTtype] = useState<string>('');
  const [plateRegistration, setPlateRegistration] = useState<boolean>(false);
  const [plateWarning, setPlateWarning] = useState<boolean>(false);
  const [plateError, setPlateError] = useState<boolean>(false);
  const [selectedFile, setSelectedFile] = useState<File>();
  const [isSelected, setIsSelected] = useState<boolean>(false);
  const [openCamera, setOpenCamera] = useState<boolean>(false);

  function checkIsValid(value: string): boolean {
    if (value.length < 8) {
      return false;
    } if (!(/[A-Z]/).test(value[0])
    && !(/[A-Z]/).test(value[1])
    && !(/[A-Z]/).test(value[3])
    && !(/[A-Z]/).test(value[4])
    && !(/[\d]/).test(value[6])
    && !(/[\d]/).test(value[7])
    ) {
      return false;
    }
    return true;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const changeHandler = (file: File) => {
    setSelectedFile(file);
    setIsSelected(true);
  };

  async function checkPlateStringSubmit() {
    if (carPlate.length <= 0) return;

    setLoadingInfo(true);
    const data = { plate: carPlate };
    checkPlateString(state.token, data)
      .then((res) => {
        setPlateTtype(res.type);
        if (res.type === 'Service') {
          setPlateInfoService(res.service);
        } else if (res.type === 'Visitor') {
          setPlateInfoVisitor(res.visitor);
        }
        setPlateInfoHouse(res.house);
        setLoadingInfo(false);
        setIsApiCalled(true);
        setPlateNotFound(false);
      })
      .catch((err) => {
        setPlateTtype('');
        if (err.response.status === 404) {
          setLoadingInfo(false);
          setIsApiCalled(true);
          setPlateNotFound(true);
        } else if (err.response.status === 403) {
          setLoadingInfo(false);
          setPlateWarning(true);
          setPlateNotFound(false);
          setIsApiCalled(false);
        } else {
          setLoadingInfo(false);
          setPlateError(true);
        }
      });
  }

  async function checkPlateImageSubmit() {
    if (!isSelected) return;

    setLoadingInfo(true);
    if (selectedFile) {
      const data = { image: selectedFile };
      checkPlateImage(state.token, data)
        .then((res) => {
          setPlateTtype(res.type);
          if (res.type === 'Service') {
            setPlateInfoService(res.service);
          } else if (res.type === 'Visitor') {
            setPlateInfoVisitor(res.visitor);
          }
          setPlateInfoHouse(res.house);
          setLoadingInfo(false);
          setIsApiCalled(true);
          setPlateNotFound(false);
        })
        .catch((err) => {
          setPlateTtype('');
          if (err.response.status === 404) {
            setLoadingInfo(false);
            setIsApiCalled(true);
            setPlateNotFound(true);
            setCarPlate(err.response.data.plate);
          } else if (err.response.status === 403) {
            setLoadingInfo(false);
            setPlateWarning(true);
            setPlateNotFound(false);
            setIsApiCalled(false);
          } else {
            setLoadingInfo(false);
            setPlateError(true);
          }
        });
    }
  }

  useEffect(() => {
    let value = carPlate.toUpperCase().replace(/[^\w]/g, '');
    if (value.match(/^(\w{2})(\w{1,2})$/)) {
      value = value.replace(/^(\w{2})(\w{1,2})$/, '$1 $2');
    } else {
      value = value.substr(0, 6).replace(/^(\w{2})(\w{2})(\w{1,2})$/, '$1 $2 $3');
    }
    setCarPlateIsValid(checkIsValid(value));
    setCarPlate(value);
  }, [carPlate]);

  return (
    <div className="app-container">
      <div className="navbar">
        <div className="left">
          <p className="logo">BravoNet</p>
        </div>
        <div className="right">
          <p>
            {state.userFirstName + state.userLastName}
          </p>
          <button
            type="button"
            className="verify"
            onClick={() => signOut()}
          >
            Logout
          </button>
        </div>
      </div>
      <div className="car-plate-container">
        <div className="left">
          <div className="input-container">
            <p className="label">Verificar patente:</p>
            <div className="input-message-container">
              <input
                type="text"
                value={carPlate}
                placeholder="AA BB 11"
                onChange={(e) => setCarPlate(e.target.value)}
              />
              <p className="warning">
                {(!carPlateIsValid && carPlate.length > 0) && 'Patente inválida (AA BB 11)'}
              </p>
            </div>
            <button
              type="button"
              className="verify"
              onClick={(carPlateIsValid) ? () => checkPlateStringSubmit() : undefined}
            >
              Revisar patente
            </button>
            <input
              type="file"
              name="file"
              onChange={(event) => (event.target.files) && changeHandler(event.target.files[0])}
            />
            <button
              type="button"
              className="camera"
              onClick={(isSelected) ? () => checkPlateImageSubmit() : () => setOpenCamera(true)}
            >
              Lector de Patentes
            </button>
          </div>
        </div>
        <div className="right">
          {(openCamera) && (
            <div className="object-container">
              <button
                className="reload-button"
                type="button"
                onClick={() => {
                  const object = document.getElementById('object');
                  if (object) {
                    object.setAttribute('data', 'http://192.168.8.155:8080');
                  }
                }}
              >
                Recargar
              </button>
                {/* eslint-disable-next-line jsx-a11y/alt-text */}
              <object
                type="text/html"
                id="object"
                data="http://192.168.8.155:8080"
                className="object"
              />
            </div>
          )}
          {(loadingInfo) && (
            <CircularProgress className="spinner" />
          )}
          {(!loadingInfo && !isApiCalled && !openCamera) && (
            <p className="title">
              Información de patente
            </p>
          )}
          {(!loadingInfo && isApiCalled && plateNotFound) && (
            <div className="information">
              <p className="title">
                Esta patente no se encuentra registrada
              </p>
              <button
                type="button"
                onClick={() => setPlateRegistration(true)}
              >
                Registrar patente
              </button>
            </div>
          )}
          {(!loadingInfo && isApiCalled && !plateNotFound && plateType === 'Resident') && (
            <div className="information">
              <p className="title">Residente</p>
              {Object.values(plateInfoHouse).map((value, i) => (
                <p
                  key={i.toString()}
                  className="house"
                >
                  {value}
                </p>
              ))}
            </div>
          )}
          {(!loadingInfo && isApiCalled && !plateNotFound && plateType === 'Visitor') && (
            <div className="information">
              <p className="title-visitor">Visita</p>
              <div className="info-visitor">
                <p className="label">Residencia: </p>
                {Object.values(plateInfoHouse).map((value, i) => (
                  <p
                    key={i.toString()}
                    className="house"
                  >
                    {value}
                  </p>
                ))}
              </div>
              <div className="information-row">
                <div className="left-visitor">
                  <div className="info-visitor">
                    <p className="label">Nombre: </p>
                    <p className="value">{`${plateInfoVisitor.firstName} ${plateInfoVisitor.lastName}`}</p>
                  </div>
                  <div className="info-visitor">
                    <p className="label">Email: </p>
                    <p className="value">{`${plateInfoVisitor.email}`}</p>
                  </div>
                  <div className="info-visitor">
                    <p className="label">Teléfono: </p>
                    <p className="value">{`${plateInfoVisitor.phone}`}</p>
                  </div>
                </div>
                <div className="left-visitor">
                  <div className="info-visitor">
                    <p className="label">Rut: </p>
                    <p className="value">{`${plateInfoVisitor.rut}`}</p>
                  </div>
                  <div className="info-visitor">
                    <p className="label">Fecha Ingreso: </p>
                    <p className="value">
                      {`${moment(plateInfoVisitor.startDate).locale('es').format('LLLL')}`}
                    </p>
                  </div>
                  <div className="info-visitor">
                    <p className="label">Fecha Salida: </p>
                    <p className="value">
                      {`${moment(plateInfoVisitor.endDate).locale('es').format('LLLL')}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          )}
          {(!loadingInfo && isApiCalled && !plateNotFound && plateType === 'Service') && (
            <div className="information">
              <p className="title-visitor">Servicio</p>
              <div className="info-visitor">
                <p className="label">Residencia: </p>
                {Object.values(plateInfoHouse).map((value, i) => (
                  <p
                    key={i.toString()}
                    className="house"
                  >
                    {value}
                  </p>
                ))}
              </div>
              <div className="info-visitor">
                <p className="label">Nombre: </p>
                <p className="value">{`${plateInfoService.name}`}</p>
              </div>
              <div className="info-visitor">
                <p className="label">Descripción: </p>
                <p className="value">{`${plateInfoService.description}`}</p>
              </div>
              <div className="info-visitor">
                <p className="label">Recurrencia: </p>
                <p className="value">
                  {plateInfoService.recurrency.split('').map((el, i) => (el === '1') && weekDays[i])}
                </p>
              </div>
            </div>
          )}
        </div>
      </div>
      {(plateRegistration) && (
        <PlateRegistrationModal
          visible={setPlateRegistration}
          carPlate={carPlate}
          state={state}
        />
      )}
      {(plateWarning) && (
        <Warning
          visible={setPlateWarning}
          message="No puede ingresar al condominio"
          buttonMessage="Volver"
        />
      )}
      {(plateError) && (
        <Warning
          visible={setPlateError}
          message="Ha habido un error, intentelo nuevamente"
          buttonMessage="Volver"
        />
      )}
    </div>
  );
}
export default CarPlateConsultView;
