import React, {
  ReactElement, useState, useContext, useEffect,
} from 'react';

import { useHistory } from 'react-router-dom';

import '../styles/LoginStyles.scss';

import {
  CircularProgress,
} from '@material-ui/core';

// Interfaces
import { ContextInterface } from '../interfaces';
import GlobalContext from '../store/GlobalContext';

// Componenst
import Warning from '../components/Warning';

// Helpers
import checkRut from '../helpers/checkRut';
import rutRegex from '../helpers/rutRegex';

function LoginView(): ReactElement {
  // Context
  const { context }: { context: ContextInterface } = useContext(GlobalContext);
  const { logIn } = context;

  // History
  const history = useHistory();

  // States
  const [rut, setRut] = useState<string>('');
  const [rutIsValid, setRutIsValid] = useState<boolean>(false);
  const [password, setPassword] = useState<string>('');
  const [loadingInfo, setLoadingInfo] = useState<boolean>(false);
  const [userIsStaff, setUserIsStaff] = useState<boolean>(true);
  const [showWarning, setShowWarning] = useState<boolean>(false);
  const [showError, setShowError] = useState<boolean>(false);

  useEffect(() => {
    const response = checkRut(rut);
    setRutIsValid(response);
  }, [rut]);

  async function checkUser() {
    if (password.length > 0) {
      setLoadingInfo(true);
      logIn(rut, password, userIsStaff)
        .then(() => {
          history.push(userIsStaff ? '/consult' : '/resident');
        })
        .catch((err) => {
          if (err.response.status === 400) {
            setShowWarning(true);
          } else {
            setShowError(true);
          }
          setLoadingInfo(false);
        });
      setLoadingInfo(false);
    }
  }

  return (
    <div className="app-container-login">
      {(showWarning) && (
        <Warning
          visible={setShowWarning}
          message="Rut y contraseña no coinciden"
          buttonMessage="Volver"
        />
      )}
      {(showError) && (
        <Warning
          visible={setShowWarning}
          message="Ha ocurrido un error, inténtelo nuevamente"
          buttonMessage="Volver"
        />
      )}
      <div className="login-container">
        <div className="header-type-user">
          <button
            type="button"
            className={userIsStaff ? 'user-type-selected' : 'user-type-not-selected'}
            onClick={() => { setUserIsStaff(true); }}
          >
            Staff
          </button>
          <button
            type="button"
            className={userIsStaff ? 'user-type-not-selected' : 'user-type-selected'}
            onClick={() => { setUserIsStaff(false); }}
          >
            Resident
          </button>
        </div>
        <div className="header">
          <p>Login</p>
        </div>

        <div className="input-container">
          <p className="label">Rut</p>
          <input
            type="text"
            name="rut"
            className="login-input"
            placeholder="Rut"
            value={rut}
            onChange={(event) => setRut(rutRegex(event.target.value))}
          />
          <p className="warning">
            {(!rutIsValid && rut.length > 0) && 'Ingrese Rut Valido'}
          </p>
        </div>

        <div className="input-container">
          <p className="label">Password</p>
          <input
            type="password"
            name="password"
            className="login-input"
            placeholder="Password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </div>
        <div>
          {(loadingInfo) && (
          <CircularProgress className="spinner" />
          )}
          {(!loadingInfo) && (
            <button
              type="button"
              className="login-btn"
              onClick={() => { checkUser(); }}
            >
              Login
            </button>
          )}
        </div>
      </div>
    </div>
  );
}
export default LoginView;
