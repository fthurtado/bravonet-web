import React, {
  ReactElement, useContext, useEffect, useState,
} from 'react';
import DateFnsUtils from '@date-io/date-fns';
import Select from 'react-select';
import moment from 'moment';

import {
  DatePicker,
  TimePicker,
  DateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';

// Icons
import {
  Dehaze, Description,
} from '@material-ui/icons';

import {
  CircularProgress,
} from '@material-ui/core';

// Styles
import '../styles/ResidentVisitSchedule.scss';

// Components
import VisitRegistrationModal from '../components/VisitRegistrationModal';
import Warning from '../components/Warning';
import Visit from '../components/Visit';
import Service from '../components/Service';

// Interfaces
import {
  ContextInterface,
  StateInterface,
  ServiceData,
  VisitorData,
  ServiceData2,
  VisitorData2,
  HouseData,
} from '../interfaces';
import GlobalContext from '../store/GlobalContext';

function ResidentVisitView(): ReactElement {
  // Context
  const { state, context }: { state: StateInterface, context: ContextInterface } = useContext(GlobalContext);
  const { signOut } = context;

  const {
    addVisitor,
    addService,
    getVisitorService,
    getResidences,
  } = context;
  const [visitors, setVisitors] = useState<VisitorData2[]>([]);
  const [services, setServices] = useState<ServiceData2[]>([]);
  const visitor1 = {
    id: 0,
    plates: [],
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    rut: '',
    startDate: '',
    endDate: '',
    createdAt: '',
    updatedAt: '',
    house: 0,
  };
  const service1 = {
    id: 0,
    plates: [],
    name: '',
    description: '',
    recurrency: '',
    createdAt: '',
    updatedAt: '',
    house: 0,
  };

  // States visits
  const [visitor, setVisitor] = useState<VisitorData2>(visitor1);
  const [selectedDateIn, handleDateInChange] = useState<Date | null>(new Date());
  const [selectedDateOut, handleDateOutChange] = useState<Date | null>(new Date());
  // States services
  const [service, setService] = useState<ServiceData2>(service1);
  const [recurrency, setRecurrency] = useState<string>('0000000');
  const [recurrency2, setRecurrency2] = useState<string[]>(recurrency.split(''));
  // States
  const [loadingInfo, setLoadingInfo] = useState<boolean>(false);
  const [isApiCalled, setIsApiCalled] = useState<boolean>(false);
  const [patentInfo, setPatentInfo] = useState<string>('');
  const [visitRegistration, setVisitRegistration] = useState<boolean>(false);
  const [selectedDate, handleDateChange] = useState<Date | null>(new Date());
  const [type, setType] = useState<string>('visitor');
  const [showWarning, setShowWarning] = useState<boolean>(false);
  const [showSuccess, setShowSuccess] = useState<boolean>(false);
  const [showRegisterError, setShowRegisterError] = useState<boolean>(false);
  const [houseIdentifier, sethouseIdentifier] = useState<string>('');
  // const [value3, onChange] = useState(new Date());

  useEffect(() => {
    getVisitorService(state.token, state.houseId)
      .then((res) => {
        setVisitors(res.visitors.sort((a, b) => {
          if (a.startDate > b.startDate) {
            return 1;
          }
          if (a.startDate > b.startDate) {
            return -1;
          }
          return 0;
        }));
        setServices(res.services);
      })
      .catch((err) => {
      });
  }, [visitRegistration, isApiCalled]);

  useEffect(() => {
    getResidences(state.token)
      .then((res) => {
        res.houses.map((e) => {
          if (e.id === state.houseId) {
            sethouseIdentifier(e.identifier);
            return false;
          }
          return false;
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  // Delete when backend is ready
  const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));

  function changeRecurrency(day: number) {
    const newRecurrency = (recurrency[day] === '0')
      ? `${recurrency.substring(0, day)}1${recurrency.substring(day + 1)}`
      : `${recurrency.substring(0, day)}0${recurrency.substring(day + 1)}`;
    setRecurrency(newRecurrency);
  }

  function handleSubmit(): void {
    if (type === 'visitor') {
      if (visitor.id < 1) {
        setShowWarning(true);
        return;
      }
      const data = {
        type,
        firstName: visitor.firstName,
        lastName: visitor.lastName,
        phone: visitor.phone,
        email: visitor.email,
        rut: visitor.rut,
        startDate: moment(selectedDateIn).format('YYYY-MM-DD HH:mm:ss'),
        endDate: moment(selectedDateOut).format('YYYY-MM-DD HH:mm:ss'),
        house: houseIdentifier,
        plate: visitor.plates[0].value,
      };
      console.log(data);
      addVisitor(state.token, data)
        .then(() => {
          setIsApiCalled(!isApiCalled);
          setShowSuccess(true);
        })
        .catch(() => {
          setShowRegisterError(true);
        });
    } else {
      setRecurrency('0000000');
      if (service.id < 1) {
        setShowWarning(true);
        return;
      }
      const data = {
        type,
        name: service.name,
        description: service.description,
        recurrency,
        house: houseIdentifier,
        plate: service.plates[0].value,
      };
      console.log(data);
      addService(state.token, data)
        .then(() => {
          setIsApiCalled(!isApiCalled);
          setShowSuccess(true);
        })
        .catch(() => {
          setShowRegisterError(true);
        });
    }
  }

  return (
    <div className="app-container">
      { showWarning ? (
        <Warning
          visible={setShowWarning}
          message="Debes seleccionar una visita"
          buttonMessage="Volver"
        />
      ) : null}
      {showRegisterError ? (
        <Warning
          visible={setShowRegisterError}
          message="A ocurrido un error con el registro, intentelo nuevamente"
          buttonMessage="Volver"
        />
      ) : null}

      {showSuccess ? (
        <Warning
          visible={setShowSuccess}
          message="Registro completado correctamente"
          buttonMessage="Aceptar"
        />
      ) : null}
      <div className="navbar">
        <div className="left">
          <p className="logo">BravoNet</p>
        </div>
        <div className="right">
          <p>
            {state.userFirstName + state.userLastName}
          </p>
          <button
            type="button"
            className="verify"
            onClick={() => signOut()}
          >
            Logout
          </button>
        </div>
      </div>
      <div className="car-plate-container">
        <div className="left">
          <div className="input-container">
            <p className="label">Selecionar tipo:</p>
            <select
              className="select"
              name="type"
              id="type"
              onChange={(e) => {
                setType(e.target.value);
                setRecurrency('0000000');
              }}
            >
              <option value="visitor">Visita</option>
              <option value="service">Servicio</option>
            </select>
            {(type === 'visitor')
              ? (
                <div className="input_box">
                  <div className="input-message-container">
                    <p className="label">
                      Seleccionar visitante:
                    </p>
                    <Select
                      className="select_box"
                      // classNamePrefix="input-message-container"
                      isSearchable
                      value={visitor.id >= 0 ? ({ value: visitor, label: visitor.firstName }) : null}
                      components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                      placeholder="Visitante..."
                      options={visitors.map((res) => ({ value: res, label: res.firstName }))}
                      onChange={(e) => { if (e) { setVisitor(e.value); } }}
                    />
                  </div>
                  <div className="input-message-container">
                    <p className="label">
                      Fecha de Ingreso:
                    </p>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <DateTimePicker
                        value={selectedDateIn}
                        onChange={handleDateInChange}
                      />
                    </MuiPickersUtilsProvider>
                  </div>
                  <div className="input-message-container">
                    <p className="label">
                      Fecha de Salida:
                    </p>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <DateTimePicker
                        value={selectedDateOut}
                        onChange={handleDateOutChange}
                      />
                    </MuiPickersUtilsProvider>
                  </div>
                  <div className="input-message-container">
                    <button
                      type="button"
                      onClick={() => handleSubmit()}
                    >
                      Agendar
                    </button>
                  </div>
                  <div className="input-message-container">
                    <button
                      type="button"
                      onClick={() => setVisitRegistration(true)}
                    >
                      Nuevo Visitante o Servicio
                    </button>
                  </div>
                </div>
              ) : (
                <div className="input_box">
                  <div className="input-message-container">
                    <p className="label">
                      Seleccionar Servicio:
                    </p>
                    <Select
                      className="select_box"
                      // classNamePrefix="input-message-container"
                      isSearchable
                      value={service.id >= 0 ? ({ value: service, label: service.name }) : null}
                      components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                      placeholder="Servicios..."
                      options={services.map((res) => ({ value: res, label: res.name }))}
                      onChange={(e) => { if (e) { setService(e.value); } }}
                    />
                  </div>
                  <div className="input-message-container">
                    <div className="horizontal_container_2">
                      <div className="left_2">
                        <p>Lunes:</p>
                        <select className="select" name="lunes" id="type" onChange={() => changeRecurrency(0)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                        <p>Martes:</p>
                        <select className="select" name="martes" id="type" onChange={() => changeRecurrency(1)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                        <p>Miercoles:</p>
                        <select className="select" name="miercoles" id="type" onChange={() => changeRecurrency(2)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                        <p>Jueves:</p>
                        <select className="select" name="jueves" id="type" onChange={() => changeRecurrency(3)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                      </div>
                      <div className="left_2">
                        <p>Viernes:</p>
                        <select className="select" name="viernes" id="type" onChange={() => changeRecurrency(4)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                        <p>Sabado:</p>
                        <select className="select" name="sabado" id="type" onChange={() => changeRecurrency(5)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                        <p>Domingo:</p>
                        <select className="select" name="domingo" id="type" onChange={() => changeRecurrency(6)}>
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="input-message-container">
                    <button
                      type="button"
                      onClick={() => handleSubmit()}
                    >
                      Agendar
                    </button>
                  </div>
                  <div className="input-message-container">
                    <button
                      type="button"
                      onClick={() => setVisitRegistration(true)}
                    >
                      Nuevo Visitante o Servicio
                    </button>
                  </div>
                </div>
              )}
          </div>
        </div>
        <div className="right">
          {(type === 'visitor')
            ? (
              <div className="object-container">
                <div className="horizontal_container">
                  <p className="label">
                    Seleccionar Fecha
                  </p>
                </div>
                <div className="horizontal_container">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DatePicker
                      value={selectedDate}
                      onChange={handleDateChange}
                    />
                  </MuiPickersUtilsProvider>
                </div>
                <p className="label">
                  Visitas Agendadas
                </p>
                {visitors.map((e) => {
                  if (moment(e.startDate).format('YYYY-MM-DD') === moment(selectedDate).format('YYYY-MM-DD')) {
                    return (
                      <Visit
                        name={e.firstName}
                        lastName={e.lastName}
                        arrive={e.startDate}
                        leave={e.endDate}
                        patent={e.plates[0].value}
                      />
                    );
                  }
                  return false;
                })}
              </div>
            ) : (
              <div className="object-container">
                <p className="label">
                  Servicios Agendados
                </p>
                {services.map((e) => {
                  return (
                    <Service
                      name={e.name}
                      recurrency={e.recurrency}
                      plate={e.plates[0].value}
                    />
                  );
                })}
              </div>
            )}
        </div>
      </div>
      {(visitRegistration) && (
        <VisitRegistrationModal
          visible={setVisitRegistration}
          state={state}
          houseId={houseIdentifier}
        />
      )}
    </div>
  );
}
export default ResidentVisitView;
